package ru.t1.artamonov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;
import ru.t1.artamonov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Display project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setProjectId(id);
        @Nullable final ProjectDTO project = projectEndpointClient.getProjectById(request).getProject();
        showProject(project);
    }

}
