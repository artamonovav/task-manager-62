package ru.t1.artamonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.listener.AbstractListener;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "arguments";

    @NotNull
    private static final String ARGUMENT = "-arg";

    @NotNull
    private static final String DESCRIPTION = "Display program arguments.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractListener abstractListener : listeners) {
            @Nullable final String argument = abstractListener.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
