package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskUnbindFromProjectRequest(
            @Nullable String token
    ) {
        super(token);
    }

    public TaskUnbindFromProjectRequest(
            @Nullable String token,
            @Nullable String projectId,
            @Nullable String taskId

    ) {
        super(token);
        this.projectId = projectId;
        this.taskId = taskId;
    }

}
