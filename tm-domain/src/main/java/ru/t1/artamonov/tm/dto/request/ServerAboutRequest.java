package ru.t1.artamonov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ServerAboutRequest extends AbstractUserRequest {

    public ServerAboutRequest(@Nullable String token) {
        super(token);
    }

}
