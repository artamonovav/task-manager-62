package ru.t1.artamonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.artamonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.artamonov.tm.comparator.NameComparator;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.artamonov.tm.exception.field.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public class TaskDTOService implements ITaskDTOService {

    @NotNull
    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO add(@Nullable TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        model.setUserId(userId);
        taskRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        taskRepository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return taskRepository.findAllByUserIdWithSort(userId, NameComparator.INSTANCE.name());
        return taskRepository.findAllByUserIdWithSort(userId, getSortType(sort));
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Override
    public long getSize() {
        return taskRepository.count();
    }

    @Override
    public long getSize(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.countByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO remove(@Nullable TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO remove(@Nullable String userId, @Nullable TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new TaskNotFoundException();
        taskRepository.deleteByUserIdAndId(userId, model.getId());
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable Collection<TaskDTO> collection) {
        if (collection == null) throw new TaskNotFoundException();
        taskRepository.deleteAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task = findOneById(id);
        taskRepository.deleteById(id);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task = findOneById(id);
        taskRepository.deleteById(id);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models) {
        if (models.isEmpty()) return new ArrayList<>();
        clear();
        add(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO update(@NotNull TaskDTO model) {
        if (model == null) throw new TaskNotFoundException();
        taskRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.saveAndFlush(task);
        return task;
    }

    @NotNull
    private String getSortType(@NotNull final Sort sort) {
        if (sort == Sort.BY_NAME) return "name";
        else if (sort == Sort.BY_STATUS) return "status";
        else return "created";
    }

}
